# Target architectures

TuxMake supports building for a set of achitectures, and they are documented
here in alphabetical order. For each architecture, we specify which toolchains
are used

Architecture | Aliases     | Description
-------------|-------------|------
arc          |             | ARC
arm64        | *aarch64*   | 64-bit ARMv8
arm          |             | 32-bit ARM
i386         |             | 32-bit X86
mips         |             | 32-bit MIPS
riscv        |             | 64-bit RISC-V
x86_64       | *amd64*     | 64-bit X86
